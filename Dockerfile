FROM nvidia/cuda:9.1-cudnn7-devel-ubuntu16.04

ENV CUDA_HOME=/usr/local/cuda

# Set the working directory to /salus
ENV SALUS_WORK_ROOT=/salus
WORKDIR /salus

# Copy the current directory contents into the container at /app
# COPY . /salus

# Setup base system repositories
# gcc/g++ 5 need to be hold otherwise will be upgraded to 5.5.0 in ubuntu-toolchain-r/test
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && (echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections) \
    && apt-get install -y --no-install-recommends software-properties-common gnupg-curl ca-certificates apt-transport-https \
    && apt-mark hold g++-5 \
    && apt-mark hold gcc-5 \
    && add-apt-repository -y ppa:ubuntu-toolchain-r/test \
    && add-apt-repository -y ppa:webupd8team/java \
    && apt-get update \
    && apt-get install -y g++-7 gcc-7 \
    && (echo 'oracle-java8-installer shared/accepted-oracle-license-v1-1 select true' | /usr/bin/debconf-set-selections) \
    && apt-get install -y oracle-java8-installer oracle-java8-set-default \
    && apt-get install -y python python-dev python-pip python-wheel \
    && apt-get install -y git curl openssl libssl-dev rsync \
    && rm -rf /var/cache/oracle-jdk8-installer /var/lib/apt/lists/* \
    && pip install -U pip

# Install spack
ENV PATH="/opt/spack/bin:/opt/spack-packages/bin:${PATH}"
COPY spack/packages.yaml /root/.spack/packages.yaml
ENV SPACK_HOME=/opt/spack SPACK_PACKAGES=/opt/spack-packages
RUN git clone https://github.com/spack/spack.git "$SPACK_HOME" \
    && spack compilers \
    && spack install cmake pkgconf \
    && spack view -d false -v add "$SPACK_PACKAGES" cmake pkgconf

# Pin packages
COPY tools /opt/bin
ENV TOOL_DIR=/opt/bin
ENV PATH="${TOOL_DIR}:${PATH}"
RUN spack-pin

# Install python dependencies
RUN pip install --trusted-host pypi.python.org invoke virtualenv

# Install docker
RUN curl -JOL 'https://download.docker.com/linux/static/stable/x86_64/docker-18.09.0.tgz' \
    && tar xvf docker-18.09.0.tgz \
    && mv docker/docker "$TOOL_DIR" \
    && rm -rf docker *.tgz

# Run app.py when the container launches
CMD ["bash"]
